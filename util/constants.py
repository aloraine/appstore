def global_constants(request):
    brand = {
        'name': 'Integrated Genome Browser',
        'logo_img': 'common/img/igb_logo.png'
    }

    return {
        'brand': brand
    }
